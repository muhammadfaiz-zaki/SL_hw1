w = randn(10,1);
X = zeros(600,10);
Y = zeros(600,1);
N = zeros(600,1);

X_10 = zeros(10,10);
N_10 = zeros(10,1);
Y_10 = zeros(10,1);

MSE_training = zeros(200,1);
MSE_testing = zeros(200,1);
MSE_training10 = zeros(200,1);
MSE_testing10 = zeros(200,1);

for counter = 1:200
  
    %%generate the required 600 samples
    for count = 1:600
            
        x = randn(10,1);
        X(count,:) = x;

        n = randn;
        N(count) = n;

        y = x'*w + n;
        Y(count) = y;
    end

%%generate 10 sample training set
    for count = 1:10

        x_10 = randn(10,1);
        X_10(count,:) = x_10;

        n_10 = randn;
        N_10(count) = n_10;

        y_10 = x_10'*w + n_10;
        Y_10(count) = y_10;
    end

    %%split 100 for training set
    x_training = X(1:100,:);
    n_training = N(1:100);
    y_training = Y(1:100);

    %%split 500 for training set
    x_testing = X(101:600,:);
    n_testing = N(101:600);
    y_testing = Y(101:600);

    w_estimate = (x_training'*x_training)\x_training'*y_training;
    w_estimate10 = (X_10'*X_10)\X_10'*Y_10;

    %%MSE

    MSE_train = 1/100*(w_estimate'*(x_training')*x_training*w_estimate - 2*y_training'*x_training*w_estimate + y_training'*y_training);
    MSE_training(counter) = MSE_train;
    
    MSE_test = 1/500*(w_estimate'*(x_testing')*x_testing*w_estimate - 2*y_testing'*x_testing*w_estimate + y_testing'*y_testing);
    MSE_testing(counter) = MSE_test;
    
    MSE_train10 = 1/10*(w_estimate10'*(X_10')*X_10*w_estimate10 - 2*Y_10'*X_10*w_estimate10 + Y_10'*Y_10);
    MSE_training10(counter) = MSE_train10;
    
    MSE_test10 = 1/500*(w_estimate10'*(x_testing')*x_testing*w_estimate10 - 2*y_testing'*x_testing*w_estimate10 + y_testing'*y_testing);
    MSE_testing10(counter) = MSE_test10;
end

AVG_MSE_training = mean(MSE_training);
AVG_MSE_testing = mean(MSE_testing);
AVG_MSE_training10 = mean(MSE_training10);
AVG_MSE_testing10 = mean(MSE_testing10);

compTable = table([AVG_MSE_training;AVG_MSE_testing],[AVG_MSE_training10;AVG_MSE_testing10],'VariableNames',{'SAMPLE_100 ','SAMPLE_10'},'RowNames',{'Training','Testing'})

