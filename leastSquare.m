function leastSquare = leastSquare(X,Y)

LS = pinv(X'*X)*X'*Y;

leastSquare = LS;

end
