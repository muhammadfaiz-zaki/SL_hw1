%%ex7
function exp7 = ex7(numSample)
MSE_training = zeros(200,10);
MSE_testing = zeros(200,1);
MSE_testingV = zeros(200,1);
MSE_testingCV = zeros(200,1);
MSE_validate = zeros(200,10);
MSE_validateCV = zeros(200,10);
Gamma100 = zeros(200,1);
Gamma100V = zeros(200,1);
Gamma100CV = zeros(200,1);


w = randn(10,1);
for counter = 1:200
    %%generate the data set
    trainingSet = createDataSet(w,numSample,10);
    trainingSetV = createDataSet(w,numSample*0.8,10);
    validationSet = createDataSet(w,numSample*0.2,10);
    
    testingSet = createDataSet(w,500,10); 
      
    n = size(trainingSet.x,1);
    folds = 5;

    rowPerFold = n/folds;
    for i = 1:10
        gamma = 10^(i-7);
        
        %%ex4
        w_estimate = (trainingSet.x' * trainingSet.x + gamma * size(trainingSet.x,1) * eye(size(trainingSet.x,2))) \ (trainingSet.x'*trainingSet.y);
        %%ex5
        w_estimateV = (trainingSetV.x' * trainingSetV.x + gamma * size(trainingSetV.x,1) * eye(size(trainingSetV.x,2))) \ (trainingSetV.x'*trainingSetV.y);
        
        %%MSE on training ex4
        MSE_train = 1/n*(w_estimate'*(trainingSet.x')*trainingSet.x*w_estimate - 2*trainingSet.y'*trainingSet.x*w_estimate + trainingSet.y'*trainingSet.y);
        MSE_training(counter,i) = MSE_train;
        
        %%MSE on validation ex5
        MSE_val = 1/size(validationSet.x,1)*(w_estimateV'*(validationSet.x')*validationSet.x*w_estimateV - 2*validationSet.y'*validationSet.x*w_estimateV + validationSet.y'*validationSet.y);
        MSE_validate(counter,i) = MSE_val;
        
        %%MSE on cross validation 5 fold ex6
        j = 1;
        for k = 1:rowPerFold:n
            if (k == 1)
                valSet = trainingSet.x(k:rowPerFold,:);
                valSet_y = trainingSet.y(k:rowPerFold,:);

                trainSet = crossV(trainingSet.x,k,rowPerFold);
                trainSet_y = crossV(trainingSet.y,k,rowPerFold);

                %%Estimate w on Training Set
                w_estimateCV = (trainSet.cv' * trainSet.cv + gamma * size(trainSet.cv,1) * eye(size(trainSet.cv,2))) \ (trainSet.cv'*trainSet_y.cv);

                %%MSE
                MSE_valCV = 1/size(valSet,1)*(w_estimateCV'*(valSet')*valSet*w_estimateCV - 2*valSet_y'*valSet*w_estimateCV + valSet_y'*valSet_y);
                MSE_validateCV(counter,i) = MSE_validateCV(counter,i) + MSE_valCV; 
            else
                valSet = trainingSet.x(k:j*rowPerFold,:);
                valSet_y = trainingSet.y(k:j*rowPerFold,:);

                trainSet = crossV(trainingSet.x,k,j*rowPerFold);
                trainSet_y = crossV(trainingSet.y,k,j*rowPerFold);

                %%Estimate w on Training Set
                w_estimateCV = (trainSet.cv' * trainSet.cv + gamma * size(trainSet.cv,1) * eye(size(trainSet.cv,2))) \ (trainSet.cv'*trainSet_y.cv);

                %%MSE
                MSE_valCV = 1/size(valSet,1)*(w_estimateCV'*(valSet')*valSet*w_estimateCV - 2*valSet_y'*valSet*w_estimateCV + valSet_y'*valSet_y);
                MSE_validateCV(counter,i) = MSE_validateCV(counter,i) + MSE_valCV;
            end
            j = j + 1;
        end
    end
    
    %best gamma ex4
    [~,index] = min(MSE_training(counter,:),[],2);
    Gamma100(counter) = 10^(index-7);
    
    %best gamma ex5
    [~,indexV] = min(MSE_validate(counter,:),[],2);
    Gamma100V(counter) = 10^(indexV-7);
    
    %best gamma ex6
    AVGCross_MSE_validateCV = MSE_validateCV/folds;
    [~,indexCV] = min(AVGCross_MSE_validateCV(counter,:),[],2);
    Gamma100CV(counter) = 10^(indexCV-7);
    
    %%RR on ex4
    w_full = (trainingSet.x' * trainingSet.x + Gamma100(counter) * size(trainingSet.x,1) * eye(size(trainingSet.x,2))) \ (trainingSet.x'*trainingSet.y);
    
    %RR on ex5
    x_training = [trainingSetV.x;validationSet.x];
    y_training = [trainingSetV.y;validationSet.y];
    
    w_fullV = (x_training' * x_training + Gamma100V(counter) * size(x_training,1) * eye(size(x_training,2))) \ (x_training'*y_training);
    
    %RR on ex6
    w_fullCV = (trainingSet.x' * trainingSet.x + Gamma100CV(counter) * size(trainingSet.x,1) * eye(size(trainingSet.x,2))) \ (trainingSet.x'*trainingSet.y);
    
    %%MSE on test ex4
    MSE_test = 1/500*(w_full'*(testingSet.x')*testingSet.x*w_full - 2*testingSet.y'*testingSet.x*w_full + testingSet.y'*testingSet.y);
    MSE_testing(counter) = MSE_test;
    
    %%MSE on test ex5
    MSE_testV = 1/500*(w_fullV'*(testingSet.x')*testingSet.x*w_fullV - 2*testingSet.y'*testingSet.x*w_fullV + testingSet.y'*testingSet.y);
    MSE_testingV(counter) = MSE_testV;
    
    %%MSE on test ex6
    MSE_testCV = 1/500*(w_fullCV'*(testingSet.x')*testingSet.x*w_fullCV - 2*testingSet.y'*testingSet.x*w_fullCV + testingSet.y'*testingSet.y);
    MSE_testingCV(counter) = MSE_testCV;
end

AVG_MSE_testing = mean(MSE_testing);
STD_MSE_testing = std(MSE_testing);

AVG_MSE_testingV = mean(MSE_testingV);
STD_MSE_testingV = std(MSE_testingV);

AVG_MSE_testingCV = mean(MSE_testingCV);
STD_MSE_testingCV = std(MSE_testingCV);

%%print
fprintf('Average Test Set Error %d ex4: %.4f\n',numSample,AVG_MSE_testing);
fprintf('Standard Deviation %d ex4: %.4f\n\n',numSample,STD_MSE_testing);

fprintf('Average Test Set Error %d ex5: %.4f\n',numSample,AVG_MSE_testingV);
fprintf('Standard Deviation %d ex5: %.4f\n\n',numSample,STD_MSE_testingV);

fprintf('Average Test Set Error %d ex6: %.4f\n',numSample,AVG_MSE_testingCV);
fprintf('Standard Deviation %d ex6: %.4f\n',numSample,STD_MSE_testingCV);
end