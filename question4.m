%part 2 (4)

%implementation of least square method

%generate data. input number of samples m, and dimension n 

X = generateData(5,3);

%sample Y labels from 1st column of X

Y = sampleY(X);

%Solve least square problem

z = leastSquare(X,Y);