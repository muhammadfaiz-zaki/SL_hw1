%%ex9

MSE_training = zeros(20,1);
MSE_testing = zeros(20,1);
MSE_trainingPF = zeros(20,13);
MSE_testingPF = zeros(20,13);
MSE_trainingAF = zeros(20,1);
MSE_testingAF = zeros(20,1);

for counter = 1:20
    %load data and split random
    data = load('boston.mat');
    n = size(data.boston,1);
    trainingSplit = ceil(2/3 * n);
    
    index = randsample(1:length(data.boston), trainingSplit);
    trainingSet = data.boston(index,:);
    
    common = ismember(data.boston,trainingSet,'rows');
    data.boston(common,:) = [];
    testingSet = data.boston;
    
    x_training = trainingSet(:,1:size(trainingSet,2)-1);
    y_training = trainingSet(:,size(trainingSet,2));
    
    x_testing = testingSet(:,1:size(testingSet,2)-1);
    y_testing = testingSet(:,size(testingSet,2));
    
    %%a) Naive Regresion
    trainingOnes = ones(size(trainingSet,1),1);
    testingOnes = ones(size(testingSet,1),1);
    
    w_estimate = (trainingOnes'*trainingOnes)\(trainingOnes'*y_training);
  
    %%MSE
    MSE_train = 1/size(trainingOnes,1)*(w_estimate'*(trainingOnes')*trainingOnes*w_estimate - 2*y_training'*trainingOnes*w_estimate + y_training'*y_training);
    MSE_training(counter) = MSE_train;
    
    MSE_test = 1/size(testingOnes,1)*(w_estimate'*(testingOnes')*testingOnes*w_estimate - 2*y_testing'*testingOnes*w_estimate + y_testing'*y_testing);
    MSE_testing(counter) = MSE_test;
    
    %%b) Linear Regression with single attribute
    for feature = 1:13
        x_trainingPerFeat = [x_training(:,feature),trainingOnes];
        x_testingPerFeat = [x_testing(:,feature),testingOnes];
        
        w_estimatePF = (x_trainingPerFeat'*x_trainingPerFeat)\(x_trainingPerFeat'*y_training);
        
        %%MSE
        MSE_trainPF = 1/size(x_trainingPerFeat,1)*(w_estimatePF'*(x_trainingPerFeat')*x_trainingPerFeat*w_estimatePF - 2*y_training'*x_trainingPerFeat*w_estimatePF + y_training'*y_training);
        MSE_trainingPF(counter,feature) = MSE_trainPF;

        MSE_testPF = 1/size(x_testingPerFeat,1)*(w_estimatePF'*(x_testingPerFeat')*x_testingPerFeat*w_estimatePF - 2*y_testing'*x_testingPerFeat*w_estimatePF + y_testing'*y_testing);
        MSE_testingPF(counter,feature) = MSE_testPF;
    end    
    
    %%c) Linear Regression on all attributes
    x_trainingAllFeat = [x_training,trainingOnes];
    x_testingAllFeat = [x_testing,testingOnes];
    
    w_estimateAF = (x_trainingAllFeat'*x_trainingAllFeat)\(x_trainingAllFeat'*y_training);
    
    %%MSE
    MSE_trainAF = 1/size(x_trainingAllFeat,1)*(w_estimateAF'*(x_trainingAllFeat')*x_trainingAllFeat*w_estimateAF - 2*y_training'*x_trainingAllFeat*w_estimateAF + y_training'*y_training);
    MSE_trainingAF(counter) = MSE_trainAF;

    MSE_testAF = 1/size(x_testingAllFeat,1)*(w_estimateAF'*(x_testingAllFeat')*x_testingAllFeat*w_estimateAF - 2*y_testing'*x_testingAllFeat*w_estimateAF + y_testing'*y_testing);
    MSE_testingAF(counter) = MSE_testAF;
    
end

%%MSE and STD for a)
AVG_MSE_training = mean(MSE_training);
STD_MSE_training = std(MSE_training);

AVG_MSE_testing = mean(MSE_testing);
STD_MSE_testing = std(MSE_testing);

%%MSE and STD for b)
AVG_MSE_trainingPF = mean(MSE_trainingPF,1);
STD_MSE_trainingPF = std(MSE_trainingPF);

AVG_MSE_testingPF = mean(MSE_testingPF,1);
STD_MSE_testingPF = std(MSE_testingPF);

%%MSE and STD for c)
AVG_MSE_trainingAF = mean(MSE_trainingAF);
STD_MSE_trainingAF = std(MSE_trainingAF);

AVG_MSE_testingAF = mean(MSE_testingAF);
STD_MSE_testingAF = std(MSE_testingAF);
