%%ex6

function exp6 = ex6(numSample)
    Gamma= zeros(10,1);
    MSE_training = zeros(1,10);
    MSE_testing = zeros(1,10);
    MSE_validate = zeros(1,10);

    w = randn(10,1);
    trainingSet = createDataSet(w,numSample,10);
    testingSet = createDataSet(w,500,10); 

    n = size(trainingSet.x,1);
    folds = 5;

    rowPerFold = n/folds;



    for g = 1:10
        gamma = 10^(g-7);
        Gamma(g) = gamma;
        j = 1;

        for i = 1:rowPerFold:n
            if (i == 1)
                valSet = trainingSet.x(i:rowPerFold,:);
                valSet_y = trainingSet.y(i:rowPerFold,:);

                trainSet = crossV(trainingSet.x,i,rowPerFold);
                trainSet_y = crossV(trainingSet.y,i,rowPerFold);

                %%Estimate w on Training Set
                w_estimate = (trainSet.cv' * trainSet.cv + gamma * size(trainSet.cv,1) * eye(size(trainSet.cv,2))) \ (trainSet.cv'*trainSet_y.cv);

                %%MSE
                MSE_train = 1/size(trainSet.cv,1)*(w_estimate'*(trainSet.cv')*trainSet.cv*w_estimate - 2*trainSet_y.cv'*trainSet.cv*w_estimate + trainSet_y.cv'*trainSet_y.cv);
                MSE_training(g) = MSE_training(g) + MSE_train;

                MSE_test = 1/500*(w_estimate'*(testingSet.x')*testingSet.x*w_estimate - 2*testingSet.y'*testingSet.x*w_estimate + testingSet.y'*testingSet.y);
                MSE_testing(g) = MSE_testing(g) + MSE_test;

                MSE_val = 1/size(valSet,1)*(w_estimate'*(valSet')*valSet*w_estimate - 2*valSet_y'*valSet*w_estimate + valSet_y'*valSet_y);
                MSE_validate(g) = MSE_validate(g) + MSE_val;            
            else
                valSet = trainingSet.x(i:j*rowPerFold,:);
                valSet_y = trainingSet.y(i:j*rowPerFold,:);

                trainSet = crossV(trainingSet.x,i,j*rowPerFold);
                trainSet_y = crossV(trainingSet.y,i,j*rowPerFold);

                %%Estimate w on Training Set
                w_estimate = (trainSet.cv' * trainSet.cv + gamma * size(trainSet.cv,1) * eye(size(trainSet.cv,2))) \ (trainSet.cv'*trainSet_y.cv);

                %%MSE
                MSE_train = 1/size(trainSet.cv,1)*(w_estimate'*(trainSet.cv')*trainSet.cv*w_estimate - 2*trainSet_y.cv'*trainSet.cv*w_estimate + trainSet_y.cv'*trainSet_y.cv);
                MSE_training(g) = MSE_training(g) + MSE_train;

                MSE_test = 1/500*(w_estimate'*(testingSet.x')*testingSet.x*w_estimate - 2*testingSet.y'*testingSet.x*w_estimate + testingSet.y'*testingSet.y);
                MSE_testing(g) = MSE_testing(g) + MSE_test;

                MSE_val = 1/size(valSet,1)*(w_estimate'*(valSet')*valSet*w_estimate - 2*valSet_y'*valSet*w_estimate + valSet_y'*valSet_y);
                MSE_validate(g) = MSE_validate(g) + MSE_val; 
            end
            j = j + 1;
        end
    end

AVGCross_MSE_training = MSE_training/folds;
AVGCross_MSE_testing = MSE_testing/folds;
AVGCross_MSE_validate = MSE_validate/folds;
S = sprintf('MSE against Gamma %d sample training Set', numSample);

%%PLot for 6a)
figure(1);
semilogx(Gamma,AVGCross_MSE_training,Gamma,AVGCross_MSE_testing, Gamma,AVGCross_MSE_validate)
xlabel('log(lambda)');
ylabel('MSE');
legend('train set', 'test set', 'validation set');
title(S);
end        
        
        