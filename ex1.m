MSE_training = zeros(200,1);
MSE_testing = zeros(200,1);
MSE_training10 = zeros(200,1);
MSE_testing10 = zeros(200,1);

for count = 1:200
    w = randn;  %weight scalar
    x = randn(600,1); %x vector
    n = randn(600,1); %n vector
    x_training = x(1:100);
    x_testing = x(101:600);
    n_training = n(1:100);
    n_testing = n(101:600);
    
    x_training10 = randn(10,1);
    n_training10 = randn(10,1);
    
    y_training10 = (w'*x_training10) + n_training10; %%y vector of 10 sample training set
    y_training = (w'*x_training) + n_training; %%training set
    y_testing = (w'*x_testing) + n_testing;  %%testing set

    %%normal equation
    w_estimate = (x_training'*x_training)\(x_training'*y_training); %%estimate of w based on 100 sample training set
    w_estimate10 = (x_training10'*x_training10)\(x_training10'*y_training10); %%estimate of w based on 10 sample training set

    %%MSE
    MSE_train = 1/100*(w_estimate'*(x_training')*x_training*w_estimate - 2*y_training'*x_training*w_estimate + y_training'*y_training);
    MSE_training(count) = MSE_train;
    
    MSE_test = 1/500*(w_estimate'*(x_testing')*x_testing*w_estimate - 2*y_testing'*x_testing*w_estimate + y_testing'*y_testing);
    MSE_testing(count) = MSE_test;
    
    MSE_train10 = 1/10*(w_estimate10'*(x_training10')*x_training10*w_estimate10 - 2*y_training10'*x_training10*w_estimate10 + y_training10'*y_training10);
    MSE_training10(count) = MSE_train10;
    
    MSE_test10 = 1/500*(w_estimate10'*(x_testing')*x_testing*w_estimate10 - 2*y_testing'*x_testing*w_estimate10 + y_testing'*y_testing);
    MSE_testing10(count) = MSE_test10;
    
end

AVG_MSE_training = mean(MSE_training);
AVG_MSE_testing = mean(MSE_testing);
AVG_MSE_training10 = mean(MSE_training10);
AVG_MSE_testing10 = mean(MSE_testing10);

compTable = table([AVG_MSE_training;AVG_MSE_testing],[AVG_MSE_training10;AVG_MSE_testing10],'VariableNames',{'SAMPLE_100 ','SAMPLE_10'},'RowNames',{'Training','Testing'})
