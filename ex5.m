Gamma= zeros(10,1);
MSE_training = zeros(200,10);
MSE_testing = zeros(200,10);
MSE_validate = zeros(200,10);
MSE_training10 = zeros(200,10);
MSE_testing10 = zeros(200,10);
MSE_validate10 = zeros(200,10);

MSE_trainingFull = zeros(200,1);
MSE_trainingFull10 = zeros(200,1);
Gamma100 = zeros(200,1);
Gamma10 = zeros(200,1);

w = randn(10,1);

for counter = 1:200
    
    trainingSet = createDataSet(w,100*0.8,10);
    trainingSet10 = createDataSet(w,10*0.8,10);
    testingSet = createDataSet(w,500,10);   
    validationSet = createDataSet(w,100*0.2,10);
    validationSet10 = createDataSet(w,10*0.2,10);

    for i = 1:10
            gamma = 10^(i-7);
            Gamma(i) = gamma;

            w_estimate = (trainingSet.x' * trainingSet.x + gamma * size(trainingSet.x,1) * eye(size(trainingSet.x,2))) \ (trainingSet.x'*trainingSet.y);
            w_estimate10 = (trainingSet10.x' * trainingSet10.x + gamma * size(trainingSet10.x,1) * eye(size(trainingSet10.x,2))) \ (trainingSet10.x'*trainingSet10.y);

            %%MSE
            MSE_train = 1/80*(w_estimate'*(trainingSet.x')*trainingSet.x*w_estimate - 2*trainingSet.y'*trainingSet.x*w_estimate + trainingSet.y'*trainingSet.y);
            MSE_training(counter,i) = MSE_train;

            MSE_test = 1/500*(w_estimate'*(testingSet.x')*testingSet.x*w_estimate - 2*testingSet.y'*testingSet.x*w_estimate + testingSet.y'*testingSet.y);
            MSE_testing(counter,i) = MSE_test;
            
            MSE_val = 1/20*(w_estimate'*(validationSet.x')*validationSet.x*w_estimate - 2*validationSet.y'*validationSet.x*w_estimate + validationSet.y'*validationSet.y);
            MSE_validate(counter,i) = MSE_val;

            MSE_train10 = 1/8*(w_estimate10'*(trainingSet10.x')*trainingSet10.x*w_estimate10 - 2*trainingSet10.y'*trainingSet10.x*w_estimate10 + trainingSet10.y'*trainingSet10.y);
            MSE_training10(counter,i) = MSE_train10;

            MSE_test10 = 1/500*(w_estimate10'*(testingSet.x')*testingSet.x*w_estimate10 - 2*testingSet.y'*testingSet.x*w_estimate10 + testingSet.y'*testingSet.y);
            MSE_testing10(counter,i) = MSE_test10;
            
            MSE_val10 = 1/2*(w_estimate10'*(validationSet10.x')*validationSet10.x*w_estimate10 - 2*validationSet10.y'*validationSet10.x*w_estimate10 + validationSet10.y'*validationSet10.y);
            MSE_validate10(counter,i) = MSE_val10;

    end
    
   %%best Gamma 
   [value,index] = min(MSE_validate(counter,:),[],2);
   [value10,index10] = min(MSE_validate10(counter,:),[],2);
  
   Gamma100(counter) = 10^(index-7);
   Gamma10(counter) = 10^(index10-7);
   
   %%full training set
   x_training = [trainingSet.x;validationSet.x];
   y_training = [trainingSet.y;validationSet.y];
   
   x_training10 = [trainingSet10.x;validationSet10.x];
   y_training10 = [trainingSet10.y;validationSet10.y];
   
   
   %%RR on full training set
   w_full = (x_training' * x_training + Gamma100(counter) * size(x_training,1) * eye(size(x_training,2))) \ (x_training'*y_training);
   w_full10 = (x_training10' * x_training10 + Gamma10(counter) * size(x_training10,1) * eye(size(x_training10,2))) \ (x_training10'*y_training10);
   
   %%MSE on full training set
   MSE_trainFull = 1/size(x_training,1)*(w_full'*(x_training')*x_training*w_full - 2*y_training'*x_training*w_full + y_training'*y_training);
   MSE_trainingFull(counter) = MSE_trainFull;
   
   MSE_trainFull10 = 1/size(x_training10,1)*(w_full10'*(x_training10')*x_training10*w_full10 - 2*y_training10'*x_training10*w_full10 + y_training10'*y_training10);
   MSE_trainingFull10(counter) = MSE_trainFull10;
  
    
    
end

AVG_MSE_training = mean(MSE_training,1);
AVG_MSE_testing = mean(MSE_testing,1);
AVG_MSE_validate = mean(MSE_validate,1);
AVG_MSE_training10 = mean(MSE_training10,1);
AVG_MSE_testing10 = mean(MSE_testing10,1);
AVG_MSE_validate10 = mean(MSE_validate10,1);

AVG_MSE_trainingFull = mean(MSE_trainingFull);
AVG_MSE_trainingFull10 = mean(MSE_trainingFull10);

AVG_Gamma100 = mean(Gamma100);
AVG_Gamma10 = mean(Gamma10);

%%print avg gamma
fprintf('Average Gamma 100:%d\n',AVG_Gamma100);
fprintf('Average Gamma 10:%d\n',AVG_Gamma10);


%%PLot for 5ai)
figure(1);
semilogx(Gamma,AVG_MSE_training,Gamma,AVG_MSE_testing, Gamma,AVG_MSE_validate)
xlabel('log(lambda)');
ylabel('MSE');
legend('train set', 'test set', 'validation set');
title('MSE against Gamma');

%%PLot for 5aii)
figure(2);
semilogx(Gamma,AVG_MSE_training10,Gamma,AVG_MSE_testing10, Gamma,AVG_MSE_validate10)
xlabel('log(lambda)');
ylabel('MSE');
legend('train set', 'test set', 'validation set');
title('MSE against Gamma 10 sample training set');

