% function generateData = generateData(m, dim)
% 
% data = [1,-1];
% 
% y = zeros(m,dim+1);
% 
% for count = 1:size(y,1)
%    y(count,1:size(y,2)-1) = datasample(data,size(y,2)-1);
% end
% 
% y(:,size(y,2)) = 1;
% generateData.y = y;
% 
% end

function createDataset = createDataSet(w, numberofSamples, dimension)

x = zeros(numberofSamples, dimension);
n = zeros(numberofSamples, 1);

for i = 1:numberofSamples
    
    x(i,:) = randn(1, dimension);
    n(i) = randn();
   
end

y = x*w + n;

createDataset.y =y;
createDataset.x =x;
createDataset.n =n;

end


