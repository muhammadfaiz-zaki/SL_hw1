function calculateMSE = mse(w,X,Y)

%%calculate mean square error

l = size(X,1);
meanSqEr = 1/l * (w' * (X') * X * w - 2 * Y' * X * w + Y' * Y);

calculateMSE = meanSqEr;
end

