function generateData = generateData(m, dim)

data = [1,-1];

y = zeros(m,dim+1);

for count = 1:size(y,1)
   y(count,1:size(y,2)-1) = datasample(data,size(y,2)-1);
end

y(:,size(y,2)) = 1;
generateData = y;

end